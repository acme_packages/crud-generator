# Laravel CRUD Generator

[![Build Status](https://travis-ci.org/acmesaico/crud-generator.svg)](https://travis-ci.org/acmesaico/crud-generator.svg)
[![Total Downloads](https://poser.pugx.org/acmesaico/crud-generator/d/total.svg)](https://packagist.org/packages/acmesaico/crud-generator)
[![Latest Stable Version](https://poser.pugx.org/acmesaico/crud-generator/v/stable.svg)](https://packagist.org/packages/acmesaico/crud-generator)
[![License](https://poser.pugx.org/acmesaico/crud-generator/license.svg)](https://packagist.org/packages/acmesaico/crud-generator)

This Generator package provides various generators like CRUD, API, Controller, Model, Migration, View for your painless development of your applications.

## Requirements
    Laravel >= 5.3
    PHP >= 5.6.4

## Installation
```
composer require acmesaico/crud-generator --dev
```

## Documentation
Go through to the [detailed documentation](doc#readme)

## Screencast

[![Screencast](http://img.youtube.com/vi/831-PFBsYfw/0.jpg)](https://www.youtube.com/watch?v=K2G3kMQtY5Y)

#### If you're still looking for easier one then try this [Admin Panel](https://github.com/acmesaico/laravel-admin)

## Author

[Acme Saico] :email: [Email Me](mailto:ahmed.sadek@acmesaico.com)

##reference

[Sohel Amin](http://www.sohelamin.com) :email: [Email Me](mailto:sohelamincse@gmail.com)

## License

This project is licensed under the MIT License - see the [License File](LICENSE) for details
